; Model of a typical forest in Brandenburg, Germany under climate change
; @author Oscar Blank <oscarblank@mail.de>
; @license Apache 2.0
; @copyright 2023 Oscar Blank
; @date 2023-06-12

__includes [
  "include/climate.nls"                   ;  Integrating climate data and procedures and a calendar
]

globals [
  local-average-temperature               ;  Average temperature in Brandenburg as a climate projection baseline
  tree-yield                              ;  Number of trees harvested during the simulation
  trees-burned                            ;  Number of trees that died due to a fire
  forest-age                              ;  The forest's age. Relevant for "clearcut-every-90-years" management strategy
  ratio-deciduous-trees
]

;------------------------------------------------------------------------------------------------------------------------------
;          Breeds
;------------------------------------------------------------------------------------------------------------------------------

; Creating turtle breeds
breed [ fires fire ]
breed [ pests pest ]
breed [ trees tree ]


turtles-own [
  vitality
  age                                    ;  In years
  max-age                                ;  Life expectancy of the turtles
]
trees-own [
  species
  climate-susceptibility                 ;  Factor how strong temperature increase impacts the vitality of the trees (float)
  flammability                           ;  Factor how fast a tree starts to burn (float)
  harvest-age                            ;  Age at which trees usually get harvested
  decomposition-time                     ;  Years since the tree died
]

;------------------------------------------------------------------------------------------------------------------------------
;          Startup, setup, and go
;------------------------------------------------------------------------------------------------------------------------------

to startup
  setup                                   ;  Sets up the model when opening the project
end

to setup
  clear-all                               ;  Reset the netlogo world
  setup-patches
  setup-tree-composition
  setup-trees
  setup-pests
  set local-average-temperature 8.7       ;  Set average temperature for Brandenburg 1961-1990 average
  setup-climate                           ;  Setup procedure used in climate.nls
  display-labels                          ;  Show or hide vitality
  reset-ticks
end

to go
  if count turtles > 10000 [ stop ]       ;  Stop simulation when exceeding too many turtles to avoid program instability
  go-climate                              ;  Go procedures used in climate.nls
  climate-impact-turtles
  become-older
  grow-trees
  eat-trees                               ;  Make pests eat trees
  reproduce                               ;  If vitality high enough, the individuals reproduce
  burn-forest                             ;  Starting a fire that kills trees
  check-death                             ;  If vitality to low, the turtles die
  manage-forest
  display-labels                          ;  Allow to switch labels on or off during the simulation

  tick
end

;______________________________________________________________________________________________________________________________
;                                                    SETUP PROCEDURES
;______________________________________________________________________________________________________________________________

;------------------------------------------------------------------------------------------------------------------------------
;          Setting up patches
;------------------------------------------------------------------------------------------------------------------------------

to setup-patches
  ask patches [
  set pcolor 38
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Setting up trees
;------------------------------------------------------------------------------------------------------------------------------

to setup-tree-composition
  if tree-composition = "pine-forest" [
    set ratio-pines 1
  ]
  if tree-composition = "average-tree-composition" [
    set ratio-pines 0.7                   ;  On average 70 % pine in forests in Brandenburg
  ]
  if tree-composition = "mixed-deciduous-forest" [
    set ratio-pines 0.1                   ;  A decidious mixed forest can have a 10 % share of coniferous trees
  ]
  set ratio-deciduous-trees 1 - ratio-pines
  set forest-age initial-forest-age
end

to setup-trees
  seed-trees
  ifelse forest-management = "harvesting-single-trees" [
    ask trees [
      set age random initial-forest-age
    ]
  ]
  [
    ask trees [
    set age initial-forest-age            ;  Most trees in Brandenburg and Berlin are between 41 and 60 years old.
  ]
  ]
  grow-trees
end

to seed-trees
  ask n-of (initial-number-trees * ratio-pines) patches with [ not any? trees-here ] [
    seed-pines
  ]
  ask n-of (initial-number-trees * ratio-deciduous-trees) patches with [ not any? trees-here ] [
    seed-deciduous-trees
  ]
end

to seed-pines
  sprout-trees 1 [
    set species "pine"
    set shape "tree scots pine"
    set color 52
    set size  0
    set vitality random-normal 50 10       ;  Normally-distributed random values with mean and standard deviation
    set climate-susceptibility 0.001
    set flammability 0.7
    set harvest-age 80 + random 40        ;  Pines usually get harvested when they are between 80 and 120 years old
    set max-age 200 + random 800          ;  Pines can become 800-1000 years old
    ]
end

to seed-deciduous-trees
  sprout-trees 1 [
    set species "deciduous"
    set shape "tree"
    set color 52 + random 5
    set size  0
    set vitality random-normal 50 10
    set climate-susceptibility 0.0001
    set flammability 0.2
    set harvest-age 100 + random 80       ;  Most deciduous tree species get harvested when they are 100-180 years old
    set max-age harvest-age + 50 + random 570     ;  Deciduous trees can become 150-1000 years old
    ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Setting up pests
;------------------------------------------------------------------------------------------------------------------------------

to setup-pests
    create-pests initial-number-pests [
    set shape "bug"
    set color black
    setxy random-xcor random-ycor
    set size 0.8
    set vitality 50
    set max-age 1000000                   ;  Since pests represent populations instead of individuals, they don't have max-age
  ]
end



;______________________________________________________________________________________________________________________________
;                                                    GO PROCEDURES
;______________________________________________________________________________________________________________________________

;------------------------------------------------------------------------------------------------------------------------------
;          Calculating climate impact on turtles
;------------------------------------------------------------------------------------------------------------------------------

to climate-impact-turtles                 ;  Impact of rising temperatures on vitality
  ask trees with [ vitality > 0 ] [
       set vitality vitality - climate-susceptibility * (temperature - local-average-temperature)
                                          ;  Minus local-average-temperature to set all temperature below to increase
      ]                                   ;  and above to decrease vitality
  ask pests [
    set vitality vitality + 0.006 * (temperature)
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Aging and growth of trees
;------------------------------------------------------------------------------------------------------------------------------

to become-older
  ask turtles [
    set age age + (1 / 52)                ;  Each week (= each tick), the turtles become 1/52 of a year older
  ]
  ask trees with [ vitality <= 0 ] [
    set decomposition-time decomposition-time + (1 / 52)
  ]
  set forest-age forest-age + (1 / 52)
end

to grow-trees
  ask trees with [ size < 2.4 and vitality > 0 ] [
    ifelse age < 96 [
      set size age / 40
    ][
      set size 2.4
    ]
    ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Pests eating trees
;------------------------------------------------------------------------------------------------------------------------------

to eat-trees
  ask patches with [ any? pests-here and any? trees-here with [ species = "pine" and vitality < 30 and age > 30 ]] [
    ask trees-here [
      set vitality vitality - tree-vitality-loss
    ]                                     ;  Decrease trees vitality through being eaten by pests
    ask pests-here with [ vitality < 100 ] [
      set vitality vitality + pest-vitality-from-eating
    ]                                     ;  Increase pests vitality from eating trees
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Fire burning trees
;------------------------------------------------------------------------------------------------------------------------------

to burn-forest
  ask trees [                             ;  When low vitality and random probability times flammability, trees burn
    if vitality < 40 and temperature > 25 and age > 40 and random 1000000 < (10 * flammability) [
      burn-tree
    ]
  ]
  ask fires [
    let burn-radius (0.1 * temperature)   ;  Temperature as a proxy for dryness, higher dryness = fire spreading further
    ask trees in-radius burn-radius [
      if random 50 > vitality [           ;  The probability of fire to spread to a tree depends partly on the tree's vitality
        burn-tree
      ]
    ]
    if count pests > min-number-pests [
      ask pests in-radius burn-radius [   ;  Fire kills pests with 90 % probability
        die
      ]
    ]
  ]
  if count trees > 0 and (count fires / count trees) > 2 [
    set forest-age 0                      ;  Reset forest-age when a fire killed most trees
  ]
end

to burn-tree
  hatch-fires 1 [
    set shape "fire"
    set color orange
    set size 1
    set age 0
    set max-age (1 / 52)                  ;  Fire dies after 1 week
    ]
  set trees-burned trees-burned + 1
  die
end

;------------------------------------------------------------------------------------------------------------------------------
;          Checking for death of turtles
;------------------------------------------------------------------------------------------------------------------------------

to check-death                            ;  Kills turtles when not enough vitality
  ask trees with [ vitality <= 0 and color != grey ] [
    set color brown
  ]
  ask turtles with [ age > max-age ] [ die ]
  if count pests > min-number-pests [
    ask pests with [ vitality <= 0 ] [ die ]
    ]
  if temperature < -12 and count pests > min-number-pests [
    ask n-of (0.8 * count pests) pests [ die ]  ;  Kills 50 % of pests at minus temperatures
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Forest management
;------------------------------------------------------------------------------------------------------------------------------

to manage-forest
  if forest-management = "harvesting-single-trees" [
    ask trees with [ age > harvest-age and vitality > 0 ] [
      set tree-yield tree-yield + 1
      ask patch-here [
        ifelse random-float 1 <= ratio-pines [
          seed-pines
        ]
        [
          seed-deciduous-trees
        ]
        ]
      die
    ]
  ]
  if forest-management = "clearcut-every-90-years" [
    if forest-age = 0 [
      seed-trees
    ]
    if forest-age > 90 [
      let harvestable-trees (count trees with [ vitality > 0 and age > 30 ]) - 10
      if harvestable-trees >= 0 [
        ask n-of harvestable-trees trees with [ vitality > 0 and age > 30 ] [
          set tree-yield tree-yield + 1   ;  Every 90 years trees older than 30 years will be harvested
          die
        ]
        set forest-age 0
      ]
    ]
  ]
  ifelse remove-dead-trees? = true [
    ask trees with [ vitality <= 0] [
      die
    ]
  ]
  [
    ask trees with [ decomposition-time > 20 ] [ die ]
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          Reproduction of trees and pests
;------------------------------------------------------------------------------------------------------------------------------

to reproduce                             ;  Lets turtles reproduce when sufficient vitality
  ask pests [
    if vitality > 55 and count pests in-radius 1 < 5 and temperature > 20 [
      set vitality 10
      hatch 1 [
        set vitality 50
        right random 360
        forward 1 + random 1
        set age 0
      ]
    ]
  ]
  if temperature < 30 and temperature > 10 and count patches with [
    not any? trees-here and not any? pests-here
  ] > 1 [
    let number-of-seeds (tree-reproduction-rate * count patches with [ not any? trees-here and not any? fires-here ])
    ask n-of (number-of-seeds * ratio-pines) patches with [
      not any? trees-here and not any? fires-here ] [
      seed-pines
  ]
    ask n-of (number-of-seeds * ratio-deciduous-trees) patches with [
      not any? trees-here and not any? fires-here ] [
      seed-deciduous-trees
  ]
  ]
end

;------------------------------------------------------------------------------------------------------------------------------
;          View settings
;------------------------------------------------------------------------------------------------------------------------------

to display-labels                        ;  Enable or disable vitality number display on the NetLogo world
  ifelse show-vitality? [
    ask turtles [ set label round vitality ]
  ]
  [
    ask turtles [ set label "" ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
200
30
668
499
-1
-1
13.94
1
10
1
1
1
0
1
1
1
-16
16
-16
16
1
1
1
weeks
30.0

BUTTON
5
155
60
188
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
65
155
125
188
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
680
30
1040
180
trees
time
trees
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"pines-living" 1.0 0 -10899396 true "" "plot count trees with [ species = \"pine\" and vitality > 0 ]"
"pines-dead" 1.0 0 -16777216 true "" "plot count trees with [ species = \"pine\" and  vitality <= 0 ]"
"deciduous-trees-living" 1.0 0 -4079321 true "" "plot count trees with [ species = \"deciduous\" and vitality > 0 ]"
"deciduous-trees-dead" 1.0 0 -6459832 true "" "plot count trees with [ species = \"deciduous\" and  vitality <= 0 ]"

PLOT
680
190
925
340
pests
time
pests
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"pests" 1.0 0 -7500403 true "" "plot count pests"

SLIDER
1195
285
1375
318
initial-number-pests
initial-number-pests
0
100
50.0
1
1
NIL
HORIZONTAL

SWITCH
1195
190
1375
223
show-vitality?
show-vitality?
1
1
-1000

MONITOR
1050
30
1115
75
trees-living
count trees with [ vitality > 0 ]
17
1
11

MONITOR
1225
30
1290
75
pests
count pests
17
1
11

SLIDER
1195
375
1375
408
pest-vitality-from-eating
pest-vitality-from-eating
0
100
15.0
1
1
NIL
HORIZONTAL

SLIDER
1195
420
1375
453
tree-vitality-loss
tree-vitality-loss
0
100
20.0
1
1
NIL
HORIZONTAL

CHOOSER
5
60
185
105
climate-scenario
climate-scenario
"smallest-plausible-increase" "medium-increase" "largest-plausible-increase"
2

MONITOR
1310
30
1375
75
fires
count fires
17
1
11

TEXTBOX
5
5
155
30
Model settings
18
0.0
1

TEXTBOX
680
5
830
30
Model analytics
18
0.0
1

MONITOR
1050
80
1115
125
NIL
year
17
1
11

MONITOR
1140
80
1205
125
NIL
month
17
1
11

MONITOR
1225
80
1290
125
NIL
day
17
1
11

MONITOR
1050
135
1115
180
temp. °C
temperature
2
1
11

PLOT
935
350
1180
500
plot-temperature
time
temperature
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"temperature" 1.0 0 -16777216 true "" "plotxy day-of-year temperature"

MONITOR
1140
135
1205
180
add.-temp
temperature-increase
2
1
11

SLIDER
5
110
185
143
start-year
start-year
2020
2100
2020.0
1
1
NIL
HORIZONTAL

CHOOSER
5
220
185
265
tree-composition
tree-composition
"pine-forest" "average-tree-composition" "mixed-deciduous-forest" "custom"
0

SLIDER
5
350
185
383
initial-number-trees
initial-number-trees
0
1000
750.0
1
1
NIL
HORIZONTAL

PLOT
680
350
925
500
fires
time
fires
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"fires" 1.0 0 -955883 true "" "plot count fires"

MONITOR
1140
30
1205
75
trees-dead
count trees with [ vitality <= 0 ]
17
1
11

CHOOSER
5
415
185
460
forest-management
forest-management
"harvesting-single-trees" "clearcut-every-90-years" "wilderness"
1

MONITOR
1225
135
1290
180
NIL
tree-yield
17
1
11

PLOT
935
190
1180
340
yield
time
trees
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"yield-trees" 1.0 0 -10899396 true "" "plot tree-yield"

SLIDER
5
310
185
343
initial-forest-age
initial-forest-age
1
500
50.0
1
1
NIL
HORIZONTAL

MONITOR
1310
80
1375
125
NIL
forest-age
1
1
11

SWITCH
5
465
185
498
remove-dead-trees?
remove-dead-trees?
1
1
-1000

SLIDER
1195
330
1375
363
min-number-pests
min-number-pests
0
100
20.0
1
1
NIL
HORIZONTAL

SLIDER
5
270
185
303
ratio-pines
ratio-pines
0
1
1.0
0.01
1
NIL
HORIZONTAL

SLIDER
1195
465
1375
498
tree-reproduction-rate
tree-reproduction-rate
0
0.1
0.008
0.001
1
NIL
HORIZONTAL

TEXTBOX
5
35
155
53
Climate scenario
14
0.0
1

TEXTBOX
5
195
155
213
Forest setup
14
0.0
1

TEXTBOX
5
390
155
408
Forest management
14
0.0
1

TEXTBOX
1195
260
1345
278
Advanced
14
0.0
1

BUTTON
130
155
185
188
go once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
1310
135
1375
180
burned
trees-burned
17
1
11

@#$#@#$#@
# Netlogo Model of a typical forest in Brandenburg (Germany) under different climate change scenarios and management strategies

## WHAT IS IT?

The model simulates a typical forest in Brandenburg under different climate change scenarios, tree compositions and management options. Bark beetles as an example of forest pests and forest fires are included to show plausible climate change impacts.

## HOW IT WORKS

For the forest model, trees, pests, and fires were chosen as breeds of agents. In the model, trees are either deciduous trees or pine. The latter makes up about 70 % of trees in Brandenburg’s forests (MLUL, 2015) and was therefore included as a genus to represent the current situation more specific than just coniferous trees. Since a variety of deciduous tree species makes up a small share of the trees in Brandenburg, they have been grouped together as deciduous trees in the model without further specification. 

### Model area
For the model area, a size of 3 ha was chosen, as this equals the average size of a private forest owned by one owner in Germany (Polley & Hennig, 2015). This is rather small and only reflects the size of a forest by one private owner, but as one of the purposes of this model is to inform forest management, 3 ha reflects the most common management unit. On that area, there will be approximately 750 trees when they are fully grown (Scholz, n.d.). 

### Calendar
The temporal basis of the model is a calendar function with temperature generator that builds on a model by Carsten Lemmen (2023). The calendar progresses one week per tick and accounts for days of each month as well as leap years. 

### Temperature and climate data
The temperature varies seasonally and is based on 8.7°C, the local 30-year mean temperature of Brandenburg in the period of 1961-1990 (Helmholtz-Zentrum Hereon, 2023b). The yearly temperature range was set to 41°C, as the temperature usually varies between -10°C and +31°C throughout the year (Weather Spark, n.d.). That temperature is increased depending on the year in the calendar of the simulation, according to the chosen climate scenario. For each of the three climate scenarios, the 30-year mean temperature increase in Brandenburg was linked to the year right in the middle of the period. In Helmoltz-Zentrum Hereon (2023a), the data was available in 5-year steps and for numerous scenarios. To reflect the bandwidth of possible climate developments, the scenario with the lowest, medium, and highest temperature increase was selected for each of the 5-year data points. For years between the 5-year data points, the temperature increase was triangulated based on the temporal distance between the closest two data points. For years beyond 2085, the temperature generator calculates the temperature as if the temperature would increase by the same slope as between 2080 and 2085. Since a change in CO2-emissions after that time can be assumed and as data uncertainty increases, temperature calculations for the time after 2100 are less meaningful and reliable. The main temporal scope the model was built for is 2020-2200. Calendar, temperature generator, and climate data code are located in the separate file climate.nls and prepared to be used in other models. The temperature is used in the model as a proxy for climate change to indicate varying climate change impacts throughout the year and connect the turtle’s behaviour to climate change. 

### Trees
In the model, each tree grows on one patch. That is not how trees grow in nature but reflects to a certain extend the way pine forests are being managed. Trees are often sown in lines and equal distances. When there are free patches, light reaches the ground and new trees start to sprout. The tree composition depends on the selection in the interface. Choosing pine-forest creates a forest with only pines, as many of the forests in Brandenburg are. Average-tree-composition creates a forest with 70 % pines, which represents the share of pines in Brandenburg (MLUL, 2015). To simplify the representation of biodiversity, the other 30 % are deciduous trees in the model, although in reality there are some other coniferous trees except pine as well. Mixed-deciduous-forest creates a forest with 10 % pines and 90 % deciduous trees, as 10 % coniferous trees are still considered a mixed deciduous forest (BMEL, n.d.). When selecting custom, any ratio can be selected with the slider right below the selector.
The default age of trees is 50, as most trees in Brandenburg are 41 to 60 years old (MLUL, 2015). Each tree has its own maximum age, after which it starts rotting. That age depends on the tree type and is otherwise generated randomly. For pines it is up to 800-1000 years (Forstliche Versuchs-und Forschungsanstalt Baden-Württemberg, 2007), for deciduous trees depending on the species between 120 (e.g., birch) to 1000 (e.g., oak) years (Deutsche Wildtier Stiftung, n.d.). After a tree died because of reaching its maximum age or loss of vitality or pests, it turns brown and stays for 20 more years until it decomposed, except if remove-dead-trees is activated.  Additionally, each tree has its own age it will usually be harvested. For pines, that is between 80 and 120 years (MLUL, 2015), for most deciduous trees between 100 and 180 years (Kreusch, n.d.), but surely lower than their maximum age. Birch, poplar, alder, and the European hornbeam are usually harvested at when they are younger than 80 years (Kreusch, n.d.), but have been excluded as they are not very common in forestry. Each tick, trees get older and grow until they reach their maximum size. The behaviour and survival of trees depends on their vitality, that decreases with high temperatures and increases with lower temperatures to reflect drought stress. Being attacked by pests decreases their vitality too. To parameterise the differences of tree species in their climate resilience, their susceptibility towards climate change is included. It is used as a factor to govern how strongly temperature changes affect the trees. Moreover, each tree type has a different flammability value to account for the differences in how fast they start to burn. Brandenburg’s typical scotch pine starts to burn much faster than deciduous trees. 

### Pests
Pests generally represent bark beetles in this model, which are in the current version only specific to pines. Of course, there are other pests specific to deciduous trees too, but to keep the model simple and to better contrast biodiversity and monoculture, only pests for pines are included. To make them visible and avoid program instability due to too many turtles, each pest represents a population of bark beetles. Therefore, their maximum age is set higher than the time span the model is intended to be used for. Each time a pest eats from a tree, preferably trees with an already low vitality, it gains vitality. When a pest exceeds a certain vitality level and the temperature is above a minimum, it reproduces. When temperatures are too low, many pests die, representing their winter mortality. 

### Fires
Vitality is not applicable for fires, but they have a maximum age of one week. A fire starts to burn, when a tree has a low vitality at high temperatures and with a certain probability that uses the tree’s flammability as a factor. The higher the flammability, the more likely a fire starts to burn under the mentioned conditions. It then spreads, where the radius depends on the temperature as a proxy for dryness. The probability of the fire to spread to a tree in its radius depends partly on that tree’s vitality. The fire kills pests too, but they come back when the fire is gone, as they would migrate from other places outside the modelled area. 

### Management strategies
Three management options are available: harvesting-single-trees, clearcut-every-90-years and wilderness. When harvesting-single-trees is selected, living trees above their harvest-age are being harvested and new trees sprout on the patch they have been on before. If the new tree is a pine or deciduous tree depends on the initial tree ratio. When clearcut-every-90-years is selected, all trees older than 30 years except 10 are being harvested. Trees younger than 30 years stay, because they will not be as profitable as the older ones. 10 are kept in the area, since usually some habitat trees are not being harvested or some need to stay to meet ecological criteria. After every harvesting, the forest’s age is set back to zero. Wilderness means no harvesting of trees. The number of trees that have been harvested since the start of the simulation is displayed as tree-yield. 


## THINGS TO NOTICE

Notice the differences in tree yield, fire frequency, and bark beetle populations when combining different setup options.

## THINGS TO TRY

Try the different climate scenarios, management options, and tree compositions.

## EXTENDING THE MODEL

Complex processes and interactions in nature are very much simplified in the model. For example, climate change is reduced to the increase in average yearly temperature, although an increase in extreme weather, regional changes to precipitation patterns, lengths of dry periods, length of the warm season might all be relevant for how climate change impacts trees, pest outbreaks, and fire risks. The model could be extended by those factors for which data is available and can somehow be tied to the tree’s behaviour in the model. Moreover, the model could be expanded by pests that are specific to trees other than pines to account for the present biodiversity of pests as well. The focus only on pests affecting pines might make it harder to compare pest effects. Also, the forest could consist of other coniferous trees than pine and deciduous trees could be differentiated into species. To make regional differences visible and to get an overview about the whole forestry in Brandenburg, one could make the model spatially explicit by integrating GIS data on forest land cover. It would than extend the boundaries beyond the current average forest and would show how a change in climate and management could play out at the federal state level. 


## NETLOGO FEATURES

Other than "ticks", NetLogo does not feature a calendar to make model behaviour connected to time and explicit. This model includes one.

## CREDITS AND REFERENCES

The model was created by Oscar Blank and includes a calendar and temperature generator from a model by Carsten Lemmen. 
See the code tab and the climate.nls file for copyright notices.


BMEL. (n.d.). Was ist ein Mischwald? Bundesministerium Für Ernährung Und Landwirtschaft- Bundeswaldinventur. Retrieved 15 July 2023, from https://www.bundeswaldinventur.de/dritte-bundeswaldinventur-2012/hintergrundinformationen/was-ist-ein-mischwald


Deutsche Wildtier Stiftung. (n.d.). Baumarten in Deutschland. Retrieved 21 August 2023, from https://www.deutschewildtierstiftung.de/baumarten-laubbaum-nadelbaum


Forstliche Versuchs-und Forschungsanstalt Baden-Württemberg. (2007). Baumartenporträt: Die Waldkiefer. https://www.waldwissen.net/de/lebensraum-wald/baeume-und-waldpflanzen/nadelbaeume/die-waldkiefer


Helmholtz-Zentrum Hereon. (2023a, January). Brandenburg, Berlin: Mögliche mittlere Änderung der durchschnittlichen Temperatur im Jahresmittel bis Ende des 21. Jahrhunderts (2071-2100) im Vergleich zu heute (1961-1990): Zunahme | Regionaler Klimaatlas Deutschland. https://www.regionaler-klimaatlas.de/klimaatlas/2071-2100/jahr/durchschnittliche-temperatur/brandenburg-berlin/mittlereanderung.html


Helmholtz-Zentrum Hereon. (2023b, January). Metropolreg. Berlin-Brandenburg: Durchschnittliche Temperatur (1961-1990). Norddeutscher Klimamonitor. https://www.norddeutscher-klimamonitor.de/klima/1961-1990/jahr/durchschnittliche-temperatur/metropolregion-berlin-brandenburg/dwd-nkdz.html


Kreusch, M. (n.d.). Umtriebszeit: Wie lange benötigt ein Baum bis zur Hiebsreife? Wald-Prinz.De. Retrieved 21 August 2023, from https://www.wald-prinz.de/umtriebszeit-wie-lange-benotigt-ein-baum-bis-zur-hiebsreife/3697


Lemmen, C. (2023, March 3). Leuphana Ecosystem Modeling 2023 / netlogo-first-steps. GitLab. https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/netlogo-first-steps


MLUL. (2015). Wälder Brandenburgs. Ergebnisse der ersten landesweiten Waldinventur (p. 36). Ministerium für Ländliche Entwicklung, Umwelt und Landwirtschaft. https://forst.brandenburg.de/sixcms/media.php/9/LWI_Broschuere2015.pdf


Polley, H., & Hennig, P. (2015). Waldeigentum im Spiegel der Bundeswaldinventur. AFZ-DerWald, 6. https://www.bundeswaldinventur.de/fileadmin/SITE_MASTER/content/Downloads/AFZ-DerWald_62015_Polley_BWI_Waldeigentum.pdf


Scholz, D. (n.d.). Schätzhilfen: Wieviel Festmeter und Bäume stehen in meinem Wald? Landwirtschaftskammer Niedersachsen. Retrieved 21 August 2023, from https://www.lwk-niedersachsen.de/lwk/news/36164_Schaetzhilfen_Wieviel_Festmeter_und_Baeume_stehen_in_meinem_Wald


Weather Spark. (n.d.). Klima für Brandenburg an der Havel, Wetter nach Monat, durchschnittliche Temperatur (Deutschland). Weather Spark. Retrieved 21 August 2023, from https://de.weatherspark.com/y/73885/Durchschnittswetter-in-Brandenburg-an-der-Havel-Deutschland-das-ganze-Jahr-%C3%BCber
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fire
false
0
Polygon -7500403 true true 151 286 134 282 103 282 59 248 40 210 32 157 37 108 68 146 71 109 83 72 111 27 127 55 148 11 167 41 180 112 195 57 217 91 226 126 227 203 256 156 256 201 238 263 213 278 183 281
Polygon -955883 true false 126 284 91 251 85 212 91 168 103 132 118 153 125 181 135 141 151 96 185 161 195 203 193 253 164 286
Polygon -2674135 true false 155 284 172 268 172 243 162 224 148 201 130 233 131 260 135 282

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

tree pine
false
0
Rectangle -6459832 true false 120 225 180 300
Polygon -7500403 true true 150 240 240 270 150 135 60 270
Polygon -7500403 true true 150 75 75 210 150 195 225 210
Polygon -7500403 true true 150 7 90 157 150 142 210 157 150 7

tree scots pine
false
0
Polygon -6459832 true false 150 195 180 165 195 165 150 210 165 180
Polygon -6459832 true false 135 195 75 135 90 135 150 195 135 195
Rectangle -6459832 true false 135 120 165 300
Circle -7500403 true true 30 90 60
Circle -7500403 true true 63 63 85
Circle -7500403 true true 120 45 60
Circle -7500403 true true 146 71 67
Circle -7500403 true true 129 114 42
Circle -7500403 true true 105 135 30
Circle -7500403 true true 189 114 42
Circle -7500403 true true 208 58 62
Circle -7500403 true true 159 129 42
Circle -7500403 true true 165 60 90

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.3.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="analysis_1" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="9360"/>
    <metric>tree-yield</metric>
    <metric>trees-burned</metric>
    <enumeratedValueSet variable="remove-dead-trees?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tree-vitality-loss">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-year">
      <value value="2020"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tree-composition">
      <value value="&quot;pine-forest&quot;"/>
      <value value="&quot;average-tree-composition&quot;"/>
      <value value="&quot;mixed-deciduous-forest&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-trees">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tree-reproduction-rate">
      <value value="0.008"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-vitality?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-vitality-from-eating">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="climate-scenario">
      <value value="&quot;smallest-plausible-increase&quot;"/>
      <value value="&quot;medium-increase&quot;"/>
      <value value="&quot;largest-plausible-increase&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-number-pests">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pests">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-forest-age">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="forest-management">
      <value value="&quot;harvesting-single-trees&quot;"/>
      <value value="&quot;clearcut-every-90-years&quot;"/>
      <value value="&quot;wilderness&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
