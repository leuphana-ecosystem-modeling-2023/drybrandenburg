[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

# Climate data and calendar integration

## Description
This file prepares and integrates climate data and a calendar into a netlogo model. It includes climate data for Brandenburg (Germany) and runs with one week per tick, but can be adapted to the specific needs in a different model. 

## Installation
Save this file named `climate.nls` in a subfolder of your netlogo model file `filename.nlogo` called `include`

In the `climate.nls` file:
- Replace temperature data for Brandenburg with temperature data for the region of your model. Data for different Germany and its federal states can be found at the [German Regional Climate Atlas](https://www.regionaler-klimaatlas.de/klimaatlas/2071-2100/jahr/durchschnittliche-temperatur/brandenburg-berlin/mittlereanderung.html). Use 30-year averages corresponding to the 5 year intervals. 

In the `filename.nlogo` file:
- Add `__includes [ "include/climate.nls" ]` to the top
- Add `local-average-temperature` to the `globals []`
- Add `setup-climate` to the setup-procedure and `go-climate` to the go-procedure
- Set `local-average-temperature` in the setup-procedure to the average temperature of 1976 (average of 1961 - 1990) of the region for your model. If you use a different data source, use the baseline year, on which the temperature increases are based on (where they equal zero in all scenarios)
- Create switcher on the interface with the variable `climate-scenario` and the options at `setup-climate-scenarios` procedure


## Usage
- Use the variable `temperature` in your model as a factor governing behaviour or as an indicator for climate change throughout the year

## Support
Issues can be submitted on [GitLab](https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/drybrandenburg/-/issues) and questions sent via [e-mail](mailto:oscarblank@mail.de).

## Contributing
Contributions are more than welcome, just open an issue or write an e-mail.

## Authors and acknowledgment
The model is based on a calendar netlogo model with temperature by Carsten Lemmen. 

Copyright: 2023 Carsten Lemmen, Oscar Blank 

## License
This work is licensed under CC-by-SA 4.0

## Project status
This project was created in the course Ecosystem Modelling at Leuphana University Lueneburg in 2023 and development will end by September 2023. However, you are more than welcome to continue development following the license requirements.
