 [![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
# Netlogo Model of a typical forest in Brandenburg (Germany) under different climate change scenarios and management strategies

## Description
The model simulates a typical forest in Brandenburg under different climate change scenarios, tree compositions and management options. Bark beetles as an example of forest pests and forest fires are included to show plausible climate change impacts.

## Installation
The model was programmed using NetLogo 6.3, it was not tested on other versions.
1. Install NetLogo from the [official website](https://ccl.northwestern.edu/netlogo/)
2. Download the model from [GitLab](https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/drybrandenburg)
3. Open the file `drybrandenburg.nlogo`

## Usage
1. Press `setup`
2. Press `go` and watch the simulation run

You can adjust the sliders and switchers to try different simulation runs and compare their differences.

## Support
Issues can be submitted on [GitLab](https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/drybrandenburg/-/issues) and questions sent via [e-mail](mailto:oscarblank@mail.de).

## Roadmap
The model could be expanded by various factors that impact tree health, such as other pests, a more detailed water availability etc. Also, the climate data integration could be expanded beyond temperature, including other climate variables relevant to forests

## Contributing
Contributions are more than welcome, just open an issue or write an e-mail.

## Authors and acknowledgment
The netlogo model was created by Oscar Blank, but it includes a climate data integration that is based on a calendar and temperature model by Carsten Lemmen. See `includes/README.md` for further information.

Copyright: 2023 Oscar Blank

## License
The model is licensed under Apache 2.0. The calendar and temperature model is licensed under CC-by-SA 4.0. 

## Project status
This project was created in the course Ecosystem Modelling at Leuphana University Lueneburg in 2023 and development will end by September 2023. However, you are more than welcome to continue development following the license requirements.